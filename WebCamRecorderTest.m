%WebCamRecorder Object Demo file

%Initialize variables
webCamName = 'HP Truevision HD'; %name of webcam on my laptop. Needs to be change for script to work
frameRate = 15; %frames per second
currentDir = pwd; %change this to where you want the video stored
filename = 'testVideo';
videoTime = 1; %recording 1 second of video

%%
%create webCamRecorder object
recorderObj = WebCamRecorder('identificationMethod','name',...
                                'webCamName',webCamName);                       

%open webcam file
recorderObj.createVideoFile(pwd,filename);

%record one second of video
recorderObj.recordVideo(videoTime,frameRate);

%close video file
recorderObj.closeVideoFile();

%Can find the video stored in the inputted path

