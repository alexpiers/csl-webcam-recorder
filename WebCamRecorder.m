classdef WebCamRecorder < handle
    %% ===================== DESCRIPTION ==================================
    % This object is used to take video of a specific length and save it to
    % a file. One object can save video to many different files as long as
    % a file is closed and then reopened with a different name.
    %
    % METHODS:
    %   createVideoFile - opens a video file with a specific file name at a
    %   specific path.
    %   recordVideo - Records x seconds of video (based on the programmable
    %   frame rate of the webcam)
    %   closeVideoFile - closes the file in question
    %
    %Create Properties
    properties
       %string with the name of the webcam you wish to use to record video.
       %Get the possible webcam information with teh webcamlist() function
       %on the command line
       webCamName
       
       %scalar containing the ID of the webcam. Alternate way of connecting
       %to it vs the name
       webCamNumID
       
       %string containing the way of connecting to the webcam. 'name' means
       %to use the name, and everything else means use the number id.
       identificationMethod
       
       %Object containing the video file information that is passed between
       %functions
       videoWriterObj
    end
    
    
    %Create Methods
    methods
        %Constructor
        function self = WebCamRecorder(varargin)
            p = inputParser;
            p.addParameter('identificationMethod','name',@ischar);
            p.addParameter('webCamName','',@ischar);
            p.addParameter('webCamNumID',0,@isscalar);
            p.parse(varargin{:});
            
            self.identificationMethod = p.Results.identificationMethod;
            self.webCamName = p.Results.webCamName;
            self.webCamNumID = p.Results.webCamNumID;
        end
        
        %Create video file (inputs: path, filename)
        function createVideoFile(self, saveFileDirectory, filename)
            %Function that creates the video file at a given path with a
            %given filename
            %INPUTS:
            %   saveFileDirectory - string of the path you wish to
            %   save the final video file in
            %   filename - string containing the filename
            totalPath = strcat(saveFileDirectory,'\', filename);
            self.videoWriterObj = VideoWriter(totalPath);
            %concatenate the path and file name and create a VideoWriter
            %object
        end
        
        %Record video (inputs: time)
        function recordVideo(self, videoLength, frameRate)
            %Function that records video for a given rate of time and saves
            %it to the file created in above function.
            %INPTUS:
            %   videoLength - target time (in seconds) of the video taken
            %   frameRate - frame rate (in fps) of the webcam. Values possible will
            %   be based on the webcam used
            
            %Set the frame rate of the file and open it (so that data can
            %be written)
            self.videoWriterObj.FrameRate = frameRate;
            self.videoWriterObj.open();
            
            %Switch structure used to determine whether to use the webcam
            %name or numerical ID. Dependent on the 'identificationMethod'
            %variable
            switch(self.identificationMethod)
                case('name')
                    camObj = webcam(self.webCamName);
                otherwise
                    camObj = webcam(self.webCamNumID);
            end
            
            %Number of iterations is the length * frame rate(seconds *
            %fps). Iterate from 1 to that number to get to the target time
            for i = 1:round(videoLength*self.videoWriterObj.FrameRate)
                %take a snapshot from the webcam. The function snapshot
                %takes a webcam option and outputs a matrix of values
                videoImg = snapshot(camObj);
                %write the image take in the snapshot to the video file
                self.videoWriterObj.writeVideo(videoImg); 
            end
        end
        
        %Close video file (inputs: VideoWriteObj)
        function closeVideoFile(self)
            self.videoWriterObj.close()  
        end
    end   
end